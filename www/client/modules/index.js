import { combineReducers } from "redux";
import reducer from "./AboutMe";
import authentication from "./Authentication";


// Combine Reducers
const Reducer = combineReducers({
	aboutMe: reducer,
	authentication: authentication
});

// Export 
export default Reducer;