import { Map } from "immutable";
import axios from "axios";
import { createAction, handleActions } from "redux-actions";


// About Me State
const initialState = Map({
	aboutme: Map({
		caption: "",
		quote: "",
		context: "",
		name: "",
		job: "",
		email: "",
		birthday: "",
		homepage: "",
		company: "",
		stack: "",
		picture: {
			is_user_image: false,
			path: ""
		}
	})
});

// Action Types
const PREPARE = "aboutme/PREPARE";
const SUCCEED = "aboutme/SUCCEED";
const FAILED = "aboutme/FAILED";

// Action Creator
export const prepare = createAction(PREPARE);
export const succeed = createAction(SUCCEED);
export const failed = createAction(FAILED);

// Reducer
const reducer = handleActions({
	[PREPARE]: (state, action) => { },
	[SUCCEED]: (state, action) => {
		return state.set("aboutme", Map({
			caption: _convertToAvailable(action.payload.data.caption),
			quote: _convertToAvailable(action.payload.data.quote),
			context: _convertToAvailable(action.payload.data.context),
			name: _convertToAvailable(action.payload.data.name),
			job: _convertToAvailable(action.payload.data.job),
			email: _convertToAvailable(action.payload.data.email),
			birthday: _convertToAvailable(action.payload.data.birthday),
			homepage: _convertToAvailable(action.payload.data.homepage),
			company: _convertToAvailable(action.payload.data.company),
			stack: _convertToAvailable(action.payload.data.stack),
			picture: {
				is_user_image: action.payload.data.is_user_image,
				path: _convertToAvailable(action.payload.data.path)
			}
		}));
	},
	[FAILED]: (state, action) => { }
}, initialState);

// Action Executor
export const executorReadAboutMe = () => {
	return (dispatch, getState) => {
		dispatch(prepare());

		axios.get("http://192.168.219.103/api/me").then( (response) => {
			dispatch(succeed(response));
		}).catch( (error) => {
			dispatch(failed(error));
		});
	};
};

// Export Reducer
export default reducer;












// ######################################
// ## Util Func						   ##
// ######################################
const _convertToAvailable = (data) => {
	const nullIf = (data) => {
		return data == null? "" : data;
	}

	return nullIf(data).length == 0? "" : data;
}