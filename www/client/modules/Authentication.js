import axios from "axios";
import { createAction, handleActions } from "redux-actions";

// Initial State
let initalState = {
	status: "INIT",
	isLoggedIn: false,
	valid: false
}

// Actions
const VALID_LOGIN = "Authentication/VALID_LOGIN";
const INIT_LOGIN = "Authentication/INIT_LOGIN";
const PREPARE_LOGIN = "Authentication/PREPARE_LOGIN";
const SUCCEED_LOGIN = "Authentication/SUCCEED_LOGIN";
const FAILED_LOGIN = "Authentication/FAILED_LOGIN";
const LOGOUT = "Authentication/LOGOUT";

// Action Creator
const valid_login = createAction(VALID_LOGIN); 
export const init_login = createAction(INIT_LOGIN);
const prepare_login = createAction(PREPARE_LOGIN);
const succeed_login = createAction(SUCCEED_LOGIN);
const failed_login = createAction(FAILED_LOGIN);
const logout = createAction(LOGOUT);

// Action Executor 
export const loginAuthenticationExecutor = (password) => {
	return (dispatch,getState) => {
		dispatch(prepare_login());

		return axios.post("http://192.168.219.103/api/authentication", {
			password: password
		})
		.then( (res) => {
			dispatch( res.data._result == 1? succeed_login() : failed_login() );

		}).catch( (err) => {
			dispatch(failed_login())
		});
	};
};

export const validLoginSession = (key) => {
	let getCookie = (key) => {
		var data = "";

		document.cookie.split(';').map((cookie,i) =>{
			var crumb = cookie.split('=');
			if (crumb[0].indexOf(key) != -1)
				data = crumb[1];
		});
		return data;
	}

	return (dispatch,getState) => {
		return new Promise( (resolve,reject) => {
			var data = window.atob(getCookie(key));

			if ( !(typeof(data) == undefined || data.length == 0) )
				dispatch(init_login(JSON.parse(data)));

			resolve();
		})
		.then( () => {
			dispatch(valid_login({valid:true}));
		});
	}
}

export const validLogoutSession = () => {
	return (dispatch,getStatus) => {
		return new Promise( (resolve,reject) => {
			dispatch(valid_login({valid:false}));
			resolve();
		})
		.then( () => {
			dispatch(logout());
			document.cookie = "key=";
		})
		.then( () => {
			dispatch(valid_login({valid:true}));
		})
	};
}

// Action Handler
const authentication = handleActions({
	[VALID_LOGIN]: (state,action) => {
		return Object.assign({}, state, {
			valid: action.payload.valid
		});	
	},
	[INIT_LOGIN]: (state,action) => {
		return Object.assign({}, state, {
			status: action.payload.status,
			isLoggedIn: action.payload.isLoggedIn
		});
	},
	[PREPARE_LOGIN]: (state,action) => {
		return Object.assign({}, state, {
			status: "PREPARE"
		});
	},
	[SUCCEED_LOGIN]: (state,action) => {
		return Object.assign({}, state, {
			status: "SUCCEED",
			isLoggedIn: true
		});
	},
	[FAILED_LOGIN]: (state,action) => {
		return Object.assign({}, state, {
			status: "FAILED",
			isLoggedIn: false
		});
	},
	[LOGOUT]: (state,action) => {
		return Object.assign({}, state, {
			status: "INIT",
			isLoggedIn: false
		});
	}
}, initalState);

// Export 
export default authentication;