import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom"

// Components
import Header from "./container/HeaderContainer";
import NotFound from "./component/NotFound";
import Home from "./component/Home";
import PostList from "./component/PostList";
import Post from "./component/Post";
import AboutMe from "./container/AboutMeContainer";
import EditProfile from "./container/EditProfileContainer";

// Action
import { validLoginSession } from "./modules/Authentication";


// Private Route
const PrivateRoute = ({component:Component,path, isLoggedIn}) => {
	return (
		<Route
			exact={true}
			path={path}
			render= {(props) => {
				return (
					isLoggedIn?
					<Component {...props} /> :
					<Redirect to="/" />
				);
			}}
		/>
	);
}

// App Component
class App extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	componentDidMount()
	{
		this.props.validLoginSession("key");
	}

	render()
	{
		return (
			<div>
				{
					this.props.valid?
					<Router>
						<div>
							<Header />
							<Switch>
								<Route exact path="/" component={Home} />
								<Route exact path="/aboutme" component={AboutMe} />
								<PrivateRoute exact path="/editprofile" component={EditProfile} isLoggedIn={this.props.isLoggedIn} />
								<PrivateRoute exact path="/posts/:key?" component={Post} isLoggedIn={this.props.isLoggedIn} />
								<PrivateRoute path="/postlist" component={PostList} isLoggedIn={this.props.isLoggedIn} />
								<Route component={NotFound} />
							</Switch>
						</div>
					</Router> :
					""
				}
			</div>
		);
	}
}

// Default props
App.defaultProps = {
	isLoggedIn: false,
	valid: false,
	validLoginSession: () => console.log("validLoginSession is not defined")
}

// PropTypes
App.propTypes = {
	isLoggedIn: PropTypes.bool,
	valid: PropTypes.bool,
	validLoginSession: PropTypes.func
}

// Connect to store
let mapStateToProps = (state) => {
	return {
		isLoggedIn: state.authentication.isLoggedIn,
		valid: state.authentication.valid
	};
}

let mapDispatchToProps = (dispatch) => {
	return {
		validLoginSession: (key) => dispatch(validLoginSession(key))
	};
}

// Connect
export default connect(mapStateToProps, mapDispatchToProps)(App);