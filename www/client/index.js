import React from "react";
import ReactDOM from "react-dom";
import thunk from "redux-thunk";
import Reducer from "./modules/index";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

// Import CSS
import "bootstrap/dist/css/bootstrap.min.css";
import "react-notifications/lib/notifications.css";
import "./static/common.css";
import "./static/header.css";
import "./static/aboutme.css";
import "./static/editprofile.css";
import "./static/postlist.css";
import "./static/post.css";
import "./static/home.css";
import "./static/notfound.css";

// Components
import App from "./App";

// Create Store
const store = createStore( Reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk) );

// Rendering
ReactDOM.render(<Provider store={store}>
					<App />
				</Provider> , document.getElementById("app"));