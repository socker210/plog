import React from "react";

/* Layout */
import { Grid, Row, Col, Panel, ListGroup, ListGroupItem } from "react-bootstrap";
/* Component */
import axios from "axios";
import PageTitle from "./PageTitle";
import { Link } from "react-router-dom";
import { NotificationContainer, NotificationManager } from "react-notifications";
import { Button, ButtonGroup, Glyphicon } from "react-bootstrap";


// Notification
const Notification = (type, msg) => {
	switch(type)
	{
		case "ERROR":
			NotificationManager.error(msg, "Error!", 2000)
			break;
	}
}

// Post List Item component
class PostListItem extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return(
			<ListGroupItem>
				<Row>
					<Col md={8} sm={8} xs={6} className="postlist-dynamic-text-container">
						<p className="postlist-postlist-item-header">{this.props.title}</p>
					</Col>
					<Col md={4} sm={4} xs={6} className="postlist-postlist-extra-info-container">
						<small className="postlist-postlist-extra-info">{this.props.modified}</small>
					</Col>
					<Col md={6} sm={6} xs={6}>
						<Link className="postlist-postlist-action postlist-postlist-action-edit" to={"/posts/"+this.props.unique}>Edit</Link>
					</Col>
					<Col md={6} sm={6} xs={6} className="postlist-postlist-action-align-right">
						<Button bsStyle="link" onClick={this.props.onDelete} className="postlist-postlist-action postlist-postlist-action-delete">Delete</Button>
					</Col>
				</Row>
			</ListGroupItem>
		);
	}
}

// Post List component
export default class PostList extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state = {
			post: []
		};
	}

	componentDidMount()
	{
		axios.get("http://192.168.219.103/api/posts")
		.then( (res) => {
			this.setState({
				post: res.data.posts.data
			});
		})
		.catch( (err) => {
			console.log(err);
		});
	}

	_onDelete(key)
	{
		if (!confirm("Are you sure?")) 
			return;

		axios.delete("http://192.168.219.103/api/post", {
			params: {
				key: key
			}
		})
		.then( (res) => {
			if ( res.data.status == "ERROR" )
			{
				Notification(res.data.status)
				return;
			}

			let { post } = this.state;
			let delIdx = -1;

			post.map( (data,i) => {
				if (data._key == key)
					delIdx = i;
			});

			this.setState({
				post: post.slice(0, delIdx).concat( post.slice(delIdx+1, post.length) )
			});

		})
		.catch( (err) => {
			console.log(err);
		});
	}

	render()
	{
		return (
			<div>
				<PageTitle title="Manage your Posts" />
				<Grid>
					<Row>
						<Col md={12} sm={12} xs={12}>
							<Link to="posts" className="postlist-newdocument">New Document</Link>
						</Col>
						<Col md={12} sm={12} xs={12}>
							<Panel className="postlist-postlist-container">
								<ListGroup fill>
									{
										this.state.post.length == 0?
										<ListGroupItem className="postlist-postlist-no-item">
											<h2>Try to write new document</h2>
										</ListGroupItem> :
										this.state.post.map( (post,i) => {
											return (
												<PostListItem title={post._title}
														  	  unique={post._key}
														  	  modified={post._last_modified_date}
														  	  is_posting={post._is_posting}
														  	  onDelete = {this._onDelete.bind(this,post._key)}
														  	  key={i} />
											);
										})
									}
								</ListGroup>
							</Panel>
						</Col>
					</Row>
				</Grid>
				<NotificationContainer />
			</div>
		);
	}
}