import React from "react";
import axios from "axios";

/* Layout */
import { Grid, Row, Col, Panel } from "react-bootstrap";
/* Component */
import { PageHeader } from "react-bootstrap";


// NoContent component
const NoContent = (props) => {
	return (
		<Grid className="home-nocontent-container">
			<Row>
				<Col md={12} sm={12} xs={12}>
					<h1>Empty Post :(</h1>
				</Col>
			</Row>
		</Grid>
	);
}

// PostShower component
const PostShower = (props) => {
	return (
		<Panel className="home-postshower-container">
			<h1 className="home-postshower-title">{props.title}</h1>
			<small className="home-postshower-subdata">{props.modifiedDate}</small>
			<hr className="home-postshower-title-splitor" />
			<div className="home-postshower-content-container" dangerouslySetInnerHTML={{__html:props.content}}>
			</div>
		</Panel>
	);
}

// Home component
export default class Home extends React.Component
{
	constructor(props)
	{
		super(props);

		//Initial Local State
		this.state = {
			post: []
		};
	}

	componentDidMount()
	{
		axios.get("http://192.168.219.103/api/home")
		.then( (res) => {
			this.setState({
				post: res.data.post
			});
		})
		.catch( (err) => {
			console.log(err);
		});
	}

	render()
	{
		return(
			<Grid className="home-container">
				<Row>
					<Col md={12} sm={12} xs={12}>
						{
							this.state.post.length == 0?
							<NoContent /> :
							this.state.post.map((post,i) => {
								return (
									<PostShower 
										title={post._title}
										content={post._content}
										modifiedDate={post._last_modified_date}
										key={i}
									/>
								);
							})
						}
					</Col>
				</Row>
			</Grid>
		);
	}
}