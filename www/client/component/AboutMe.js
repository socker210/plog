import React from "react";
import PropTypes from "prop-types";

/* Layout */
import { Grid, Row, Col, Panel, ListGroup, ListGroupItem } from "react-bootstrap";


// Introduction Component
const Introduction = (props) => {

	const ProfilePicture = () => {
		let style = props.picture.is_user_image? { 
			backgroundImage: "url("+props.picture.path+")",
			backgroundPosition: "center",
			backgroundRepeat: "no-repeat",
			backgroundSize: "cover"} 
			: {};

		return (
			<div className="aboutme-introduction-image" style={style}></div>
		);
	}

	return (
		<div>
			<Row>
				<Col md={2} sm={3} xs={12}>
					<div className="aboutme-introduction-image-container">
						<ProfilePicture />
					</div>
				</Col>
				<Col md={10} sm={9} xs={12}>
					<div className="about-introduction-welcome-contaniner">
						<h1 className="about-introduction-welcome-title">{props.caption}</h1>
						<hr />
						<p className="about-introduction-welcome-quote"><em>{props.quote}</em></p>
					</div>
				</Col>
			</Row>
		</div>
	);
}

// Context Quote Component
const ContextQuote = (props) => {

	const ContextListGroupItem = ({header, children}) => {
		return (
			<ListGroupItem header={header} 
						   className="aboutme-contextquote-profile-children-container" 
						   children={children} />
		);
	}

	return (
		<Row>
			<Col md={12} sm={12} xs={12}>
				<div className="aboutme-contextquote-context-container">
					<p className="aboutme-contextquote-context-p">{props.context}</p>
				</div>
			</Col>
			<Col md={12} sm={12} xs={12}>
				<div>
					<h3 className="aboutme-contextquote-profile-h3">Profile</h3>
					<Panel>
						<ListGroup fill>
							<ContextListGroupItem header="name" children={props.name} />
							<ContextListGroupItem header="Birthday" children={props.birthday} />
							<ContextListGroupItem header="Job" children={props.job} />
							<ContextListGroupItem header="Company" children={props.company} />
							<ContextListGroupItem header="E-mail" children={props.email} />
							<ContextListGroupItem header="Homepage" children={props.homepage} />
							<ListGroupItem header="Stack" className="aboutme-contextquote-profile-children-container">
								{props.stack.split(',').map((item,key) => { 
									if ( item.length > 0 )
										return (<code key={key} className="aboutme-contextquote-profile-code">{item}</code>)  
								})}
							</ListGroupItem>
						</ListGroup>
					</Panel>
				</div>
			</Col>
		</Row>
	);
}

// About Me Component
class AboutMe extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	componentDidMount()
	{
		this.props.executorReadAboutMe();
	}

	render()
	{
		return (
			<div>
				<div className="aboutme-introduction-container">
					<Grid>
						<Row>
							<Col md={12} sm={12} xs={12}>
								<Introduction caption={this.props.caption}
									  		  quote={this.props.quote}
									  		  picture={this.props.picture} />
							</Col>
						</Row>
					</Grid>
				</div>
				<div>
					<Grid>
						<Row>
							<Col md={12}>
								<ContextQuote context={this.props.context} 
											  name={this.props.name}
											  job={this.props.job}
											  email={this.props.email}
											  birthday={this.props.birthday}
											  homepage={this.props.homepage}
											  company={this.props.company}
											  stack={this.props.stack} />
							</Col>
						</Row>
					</Grid>
				</div>
			</div>
		);
	}
}

// Set Default value
AboutMe.defaultProps = {
	caption: "",
	quote: "",
	context: "",
	name: "",
	job: "",
	email: "",
	birthday: "",
	homepage: "",
	company: "",
	stack: "",
	picture: {
		is_user_image: false,
		path: ""
	},
	executorReadAboutMe: () => console.log("executorReadAboutMe is not defined")
};

// Set property types
AboutMe.propTypes = {
	caption: PropTypes.string,
	quote: PropTypes.string,
	context: PropTypes.string,
	name: PropTypes.string,
	job: PropTypes.string,
	email: PropTypes.string,
	birthday: PropTypes.string,
	homepage: PropTypes.string,
	company: PropTypes.string,
	stack: PropTypes.string,
	picture: PropTypes.object,
	executorReadAboutMe: PropTypes.func
};

// Export 
export default AboutMe;