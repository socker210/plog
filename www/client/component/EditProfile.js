import React from "react";
import PropTypes from "prop-types";
import axios from "axios";

/* Layout */
import { Grid, Row, Col, Panel } from "react-bootstrap";
/* Components */
import PageTitle from "./PageTitle";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { NotificationContainer, NotificationManager } from "react-notifications";


// Notification
const Notification = (type, msg) => {
	var title = "Notification";
	var delay = 2000;

	switch(type)
	{
		case "SUCCEED":
			NotificationManager.success(msg, title, delay);
			break;
		case "FAILED":
		case "ERROR":
			NotificationManager.error(msg, title, delay);
			break;
	}
}

// Input Component
const InputText = (props) => {
	return (
		<FormGroup controlId={props.controlId}>
			<ControlLabel>{props.header}</ControlLabel>
			{
				props.textarea ?

				<FormControl componentClass="textarea"
						 	 value={props.value}
						 	 onChange={props.onUpdate}
						 	 className="editprofile-input-textarea"
						 	 placeholder={props.hint} /> :

				<FormControl type="text"
						 	 value={props.value}
						 	 onChange={props.onUpdate}
						 	 placeholder={props.hint} />
			}
		</FormGroup>
	);
}

// Image
class ProfilePicture extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	shouldComponentUpdate(nextProps, nextState)
	{
		return this.props.path !== nextProps.path;
	}

	render()
	{
		let style = this.props.is_user_image? {
			backgroundImage:"url("+this.props.path+")",
			backgroundRepeat: "no-repeat",
			backgroundSize: "cover",
			backgroundPosition: "center"} : 
			{};

		return (
			<div className="editprofile-image" onClick={ this.props.open } style={style}></div>
		);
	}
}

// Edit Profile Component
class EditProfile extends React.Component
{
	constructor(props)
	{
		super(props);

		// Declare Local State
		this.state = {
			caption: "",
			quote: "",
			context: "",
			name: "",
			birthday: "",
			job: "",
			company: "",
			email: "",
			homepage: "",
			stack: "",
			picture: {
				is_user_image: false,
				path: ""
			}
		};
	}

	componentDidMount()
	{
		this.props.executorReadAboutMe();
	}

	componentWillReceiveProps(nextProps)
	{
		// Initial Local State
		this.setState({
			caption: nextProps.caption,
			quote: nextProps.quote,
			context: nextProps.context,
			name: nextProps.name,
			birthday: nextProps.birthday,
			job: nextProps.job,
			company: nextProps.company,
			email: nextProps.email,
			homepage: nextProps.homepage,
			stack: nextProps.stack,
			picture: {
				is_user_image: nextProps.picture.is_user_image,
				path: nextProps.picture.path
			}
		});
	}

	_onUpdate(e)
	{
		let newObj = this.state;

		newObj[e.target.id] = e.target.value;
		this.setState(newObj);
	}

	_handlePOST(params, succeed, error)
	{
		axios.post("http://192.168.219.103/api/me", params)
		.then( (response) => succeed(response) )
		.catch( (err) => error(err) );
	}

	_handlePUT(params, succeed, error)
	{
		axios.put("http://192.168.219.103/api/me", params)
		.then( (response) => succeed(response) )
		.catch( (err) => error(err) );	
	}

	_onSubmit(e)
	{
		e.preventDefault();

		let params = {
			caption: this.state.caption,
			quote: this.state.quote,
			context: this.state.context,
			name: this.state.name,
			birthday: this.state.birthday,
			job: this.state.job,
			company: this.state.company,
			email: this.state.email,
			homepage: this.state.homepage,
			stack: this.state.stack,
		};

		this._handlePOST(params, (res) => {
			Notification(res.data.status, res.data.msg);
		}, (e) =>{ 
			Notification("ERROR", "Error!");
		});
	}s

	_changePicture(e)
	{
		/*
			## Supported File type

			47494638 gif
			FFD8FFDB jpg & jpeg
			FFD8FFE0 jpg & jpeg
			FFD8FFE1 jpg & jpeg
			89504E47 png
		*/

		let blobReader = new FileReader();
		let file = e.target.files[0];
		let supportedType = ["47494638","FFD8FFDB","FFD8FFE0","FFD8FFE1","89504E47"];

		blobReader.onloadend = (e) => 
		{
			switch( e.target.readyState )
			{
				case FileReader.LOADING: break;
				case FileReader.DONE:
					let uint = new Uint8Array(e.target.result);
					let hex = "";

					uint.forEach( (e) => { hex += e.toString(16).toUpperCase(); });
					
					if ( supportedType.indexOf(hex) !== -1 )
					{
						let imgReader = new FileReader();

						imgReader.onloadend = (e) => {
							this._updateImage(e.target.result);
						};
						imgReader.readAsDataURL(file);
					}
					else alert("You can upload image");
					break;
			}
		}

		blobReader.readAsArrayBuffer(file.slice(0, 4));
	}

	_updateImage(img)
	{
		let params = {
			action: "UPLOAD",
			img: img
		}

		this._handlePUT(params, (res) => { 
			this.setState( Object.assign({}, this.state, {
				picture: {
					is_user_image: true,
					path: img
				}
			}));
		}, 
		(e) => { console.log(e) });
	}

	_deleteImage()
	{
		let params = {
			action: "DELETE"
		}

		this._handlePUT(params, (res) => { 
			this.setState( Object.assign({}, this.state, {
				picture: {
					is_user_image: false,
					path: ""
				}
			}));
		}, 
		(e) => { console.log(e) });
	}

	render()
	{
		return (
			<div>
				<PageTitle title="Edit your profile" />
				<form onSubmit={this._onSubmit.bind(this)}>
					<Grid>
						<Row>
							<Col md={2} sm={3} xs={12}>
								<div className="editprofile-image-container">
									<input type="file" 
										   style={ {display: "none"} } 
										   ref={ (ref) => this.imageUploader = ref }
										   onChange={this._changePicture.bind(this)} />

									<ProfilePicture is_user_image={this.state.picture.is_user_image}
													path={this.state.picture.path}
													open={ () => this.imageUploader.click() } />
								</div>
								<div className="editprofile-button-container">
									<Button children="Remove picture"
											className="editprofile-button"
											onClick={ this._deleteImage.bind(this) } />
								</div>
							</Col>
							<Col md={10} sm={9} xs={12}>
								<div className="editprofile-form-container">
									<InputText controlId="caption" 
											   header="Caption" 
											   hint="Caption" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.caption} />

									<InputText controlId="quote" 
											   header="Quote" hint="Quote" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.quote} />

									<InputText controlId="context" 
											   header="Context" 
											   hint="Context" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.context} 
											   textarea={true} />

									<InputText controlId="name" 
											   header="Name" 
											   hint="Name" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.name} />

									<InputText controlId="birthday" 
											   header="Birthday" 
											   hint="Birthday" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.birthday} />

									<InputText controlId="job" 
											   header="Job" 
											   hint="Job" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.job} />

									<InputText controlId="company" 
											   header="Company" 
											   hint="Company" 
											   onUpdate = {this._onUpdate.bind(this)} 
											   value={this.state.company} />

									<InputText controlId="email" 
											   header="E-mail" 
											   hint="E-mail" 
											   onUpdate = {this._onUpdate.bind(this)}
											   value={this.state.email} />

									<InputText controlId="homepage" 
											   header="Homepage" 
											   hint="Homepage" 
											   onUpdate = {this._onUpdate.bind(this)}
											   value={this.state.homepage} />

									<InputText controlId="stack" 
											   header="Stack" 
											   hint="Stack" 
											   onUpdate = {this._onUpdate.bind(this)}
											   value={this.state.stack} />
								</div>
							</Col>
						</Row>
					</Grid>
					<Grid>
						<Row>
							<Col md={12} sm={12} xs={12}>
								<div className="editprofile-save-container">
									<Button type="submit" bsStyle="primary">Save Profile</Button>
								</div>
							</Col>
						</Row>
					</Grid>
				</form>
				<NotificationContainer />
			</div>
		);
	}
}

// Set Default value
EditProfile.defaultProps = {
	caption: "",
	quote: "",
	context: "",
	name: "",
	birthday: "",
	job: "",
	company: "",
	email: "",
	homepage: "",
	stack: "",
	picture: {
		is_user_image: false,
		path: ""
	},

	executorReadAboutMe: () => console.log("executorReadAboutMe is not defined")
};

// Set Prop types
EditProfile.propTypes = {
	caption: PropTypes.string,
	quote: PropTypes.string,
	context: PropTypes.string,
	name: PropTypes.string,
	birthday: PropTypes.string,
	job: PropTypes.string,
	company: PropTypes.string,
	email: PropTypes.string,
	homepage: PropTypes.string,
	stack: PropTypes.string,
	path: PropTypes.object,
	executorReadAboutMe: PropTypes.func
};

// Export 
export default EditProfile; 