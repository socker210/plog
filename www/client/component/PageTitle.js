import React from "react";
import { Grid, Row, Col } from "react-bootstrap";


export default class PageTitle extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return (
			<div className="pageheader-container">
				<Grid>
					<Row>
						<Col md={12} sm={12} xs={12}>
							<h2 className="pageheader-title">{this.props.title}</h2>
							<hr className="pageheader-line" />
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}