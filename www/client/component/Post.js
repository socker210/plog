import React from "react";

/* Layout */
import { Grid, Row, Col } from "react-bootstrap";
/* Component */
import PageTitle from "./PageTitle";
import CKEditor from "./CKEditor";
import axios from "axios";
import { Link } from "react-router-dom";
import { NotificationContainer, NotificationManager } from "react-notifications"
import { ControlLabel, FormGroup, FormControl, Button } from "react-bootstrap";


// Notification
const Notification = (type, msg) => {
	var title = "Notification";
	var delay = 2000;

	switch( type )
	{
		case "SUCCEED":
			NotificationManager.success(msg, title, delay);
			break;
		case "ERROR":
			NotificationManager.error(msg, title, delay);
			break;
	}
}

// Post Component
export default class Post extends React.Component
{
	constructor(props)
	{      
		super(props);

		// Initial State
		this.state = {
			key: "",
			title: "",
			content: "",
			is_posting: false,
			loading: true
		};
	}

	componentDidMount()
	{
		if (this.props.match.params.key != undefined)
		{
			axios.get("http://192.168.219.103/api/post", {
				params: {
					key: this.props.match.params.key
				}
			})
			.then( (res) => {
				this.setState( Object.assign({}, this.state, {
					key: this.props.match.params.key,
					title: res.data._title,
					content: res.data._content,
					is_posting: res.data._is_posting,
					loading: false
				}))
			})
			.catch( (err) => {
				console.log(err);
			});
		}
		else
		{
			this.setState( Object.assign({}, this.state, {
				loading: false
			}));
		}
	}

	_onWriteTitle(e)
	{
		this.setState(Object.assign({}, this.state, {
			title: e.target.value
		}));
	}

	_onWriteContent(e)
	{
		this.setState(Object.assign({}, this.state, {
			content: e.editor.getData()
		}));
	}

	_save()
	{		
		axios.post("http://192.168.219.103/api/post", {
			key: this.state.key,
			title: this.state.title,
			content: this.state.content,
			is_posting: true
		})
		.then( (res) => {
			this.setState( Object.assign({}, this.state, {
				key: res.data.post_id
			}));

			Notification(res.data.status, res.data.msg);
		})
		.catch( (err) => {
			console.log(err);
		});
	}

	render()
	{
		return (
			<div>
				<PageTitle title="Post your knowledge" />
				<Grid>
					<Row>
						<Col md={12} sm={12} xs={12} className="post-action-container">
							<Col md={6} sm={6} xs={6} className="post-action-left-align-container">
								<Button className="post-action" onClick={this._save.bind(this)}>Save</Button>
							</Col>
							<Col md={6} sm={6} xs={6} className="post-action-right-align-container">
								<Link className="postlist-right-close" to="/postlist">Close</Link>
							</Col>
						</Col>
						<Col md={12} sm={12} xs={12}>
							<FormGroup controlId="title">
								<FormControl type="text" placeholder="(No Title)" onChange={this._onWriteTitle.bind(this)} value={this.state.title} />
							</FormGroup>
						</Col>
						<Col md={12} sm={12} xs={12} className="post-editor-container">
							{
								this.state.loading? 
								<b>Loading</b> :
								<CKEditor content={this.state.content} 
									  	  onChange={this._onWriteContent.bind(this)} />
							}
						</Col>
					</Row>
				</Grid>
				<NotificationContainer />
			</div>
		);
	}
}