import React from "react";


// CKEDitor Component
export default class CKEditor extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	componentDidMount()
	{
		var config = {
			contentsCss: ["http://192.168.219.103/asset/static/bootstrap.min.css","http://192.168.219.103/asset/static/ckeditor.css"],
			height: 600,
			toolbar: [
				{ name: 'document', items: [ 'Source','Undo','Redo'] },
				{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike','NumberedList', 'BulletedList','Blockquote','Link'] },
				{ name: 'insert', items: [ 'Image'] },
				{ name: 'style', items: [ 'Styles' ] }
			]
		};

		CKEDITOR.replace('editor', config);
		CKEDITOR.instances.editor.on("change",this.props.onChange);
	}

	shouldComponentUpdate(nextProps, nextState)
	{
		return this.props.content !== nextProps.content;
	}

	render()
	{
		return (
			<textarea name="editor" defaultValue={this.props.content}></textarea>
		);
	}
}