import React from "react";
import PropTypes from "prop-types";
import { Navbar, Nav, NavItem, Button, ButtonToolbar, Modal, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";

// Login Component
class Login extends React.Component
{
	constructor(props, context)
	{
		super(props, context);

		// Initial local state
		this.state = {
			show: false,
			pwd: ""
		};
	}

	onShowLoginForm()
	{
		this.onHide();
		this.props.initLogin();
	}

	onHide()
	{
		this.setState({
			show: !this.state.show,
			pwd: ""
		});
	}

	onUpdatePasswordState(e)
	{
		this.setState( Object.assign({}, this.state, {
			[e.target.id]: e.target.value
		}));
	}

	onUserAuthentication()
	{
		this.props.loginAuthenticationExecutor(this.state.pwd)
		.then( () => {
			if (this.props.isLoggedIn)
			{
				this.onHide();
				let cookie = {
					status: this.props.status,
					isLoggedIn: this.props.isLoggedIn
				}

				document.cookie = "key="+window.btoa(JSON.stringify(cookie));
			}
		});
	}

	onLogOut()
	{
		this.props.validLogoutSession();
	}

	render()
	{
		// Logged In form
		const LoggedIn = () => {
			return (
				<div className="header-header-link-container">
					<Link className="header-header-link-item" to="/editprofile" onClick={this.props.onUpdateScrollPos}>Edit Profile</Link>
					<Link className="header-header-link-item" to="/postlist" onClick={this.props.onUpdateScrollPos}>Post</Link>
					<Button bsStyle="link" className="header-header-link-item" onClick={this.onLogOut.bind(this)}>Log out</Button>
				</div>		
			);
		}

		// Log in form
		const LogIn = (props) => {
			return (
				<div className="header-header-link-container">
					<Button bsStyle="link" className="header-header-link-item" onClick={this.onShowLoginForm.bind(this)}>Log in</Button>
				</div>
			);
		}

		return (
			<div className="header-header-link-container">
				{
					this.props.isLoggedIn? <LoggedIn /> : <LogIn />
				}
				<Modal show={this.state.show} onHide={this.onHide.bind(this)}>
					<Modal.Header closeButton>
						<h4>Login</h4>
					</Modal.Header>
					<Modal.Body>
						<FormGroup controlId="pwd">
							<ControlLabel>
								{
									this.props.status == "PREPARE"?
									"Loading ..." : this.props.status == "FAILED"?
									"Failed" : "Input your password"
								}
							</ControlLabel>
							<FormControl type="password" value={this.state.pwd} onChange={this.onUpdatePasswordState.bind(this)} />
						</FormGroup>
					</Modal.Body>
					<Modal.Footer>
						<Button bsStyle="primary" onClick={this.onUserAuthentication.bind(this)} disabled={this.props.status == "PREPARE"}>Login</Button>
						<Button onClick={this.onHide.bind(this)}>Close</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

// Header Component
export default class Header extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	_updateScrollPos()
	{
		window.scrollTo(0,0);
	}

	render()
	{
		return (
			<Navbar className="header-header-navbar" fixedTop={true}>
				<Navbar.Header className="header-header-navbar-logo">
					<Navbar.Brand>
						<Link className="header-header-link-title" to="/" onClick={this._updateScrollPos.bind(this)}>PLOG</Link>
					</Navbar.Brand>
				</Navbar.Header>
				<div className="header-header-link-item-container">
					<Link className="header-header-link-item" to="/aboutme" onClick={this._updateScrollPos.bind(this)}>About Me</Link>
					<Login onUpdateScrollPos={this._updateScrollPos.bind(this)} 
						   status={this.props.status} 
						   isLoggedIn={this.props.isLoggedIn}
						   initLogin={this.props.initLogin}
						   validLogoutSession={this.props.validLogoutSession}
						   loginAuthenticationExecutor={this.props.loginAuthenticationExecutor} />
				</div>
			</Navbar>
		);
	}
}

// Default Props
Header.defaultProps = {
	status: "INIT",
	isLoggedIn: false,
	initLogin: () => console.log("initLogin is not defined"),
	validLogoutSession: () => console.log("validLogoutSession is not defined"),
	loginAuthenticationExecutor: () => console.log("loginAuthenticationExecutor is not defined")
};

// Prop Types
Header.propTypes = {
	status: PropTypes.string,
	isLoggedIn: PropTypes.bool,
	initLogin: PropTypes.func,
	validLogoutSession: PropTypes.func,
	loginAuthenticationExecutor: PropTypes.func
}