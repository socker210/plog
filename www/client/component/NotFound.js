import React from "react";
import { Grid, Row, Col } from "react-bootstrap";


export default class NotFound extends React.Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return (
			<Grid>
				<Row>
					<Col md={12} sm={12} xs={12} className="notfound-container">
						<h1>Page are not found</h1>
					</Col>
				</Row>
			</Grid>
		);
	}
}