import { connect } from "react-redux";
import { executorReadAboutMe } from "../modules/AboutMe";
import EditProfile from "../component/EditProfile";


// Mapping State to Property
let mapStateToProps = (state) => {
	return {
		caption: state.aboutMe.getIn(["aboutme", "caption"]),
		quote: state.aboutMe.getIn(["aboutme", "quote"]),
		context: state.aboutMe.getIn(["aboutme", "context"]),
		name: state.aboutMe.getIn(["aboutme", "name"]),
		birthday: state.aboutMe.getIn(["aboutme", "birthday"]),
		job: state.aboutMe.getIn(["aboutme", "job"]),
		company: state.aboutMe.getIn(["aboutme", "company"]),
		email: state.aboutMe.getIn(["aboutme", "email"]),
		homepage: state.aboutMe.getIn(["aboutme", "homepage"]),
		stack: state.aboutMe.getIn(["aboutme", "stack"]),
		picture: state.aboutMe.getIn(["aboutme", "picture"])
	};
};

// Mapping function to Propery
let mapDispatchToProps = (dispatch) => {
	return {
		executorReadAboutMe: () => dispatch(executorReadAboutMe())
	};
};

// Connect to component
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);