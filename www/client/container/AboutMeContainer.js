import AboutMe from "../component/AboutMe";
import { executorReadAboutMe } from "../modules/AboutMe";
import { connect } from "react-redux";


// Mapping State to Property
let mapStateToProps = (state) => {

	return {
		caption: state.aboutMe.getIn(["aboutme", "caption"]),
		quote: state.aboutMe.getIn(["aboutme", "quote"]),
		context: state.aboutMe.getIn(["aboutme", "context"]),
		name: state.aboutMe.getIn(["aboutme", "name"]),
		job: state.aboutMe.getIn(["aboutme", "job"]),
		email: state.aboutMe.getIn(["aboutme", "email"]),
		birthday: state.aboutMe.getIn(["aboutme", "birthday"]),
		homepage: state.aboutMe.getIn(["aboutme", "homepage"]),
		company: state.aboutMe.getIn(["aboutme", "company"]),
		stack: state.aboutMe.getIn(["aboutme", "stack"]),
		picture: state.aboutMe.getIn(["aboutme", "picture"])
	};
};

// Mapping dispatch to Property
let mapDispatchToProps = (dispatch) => {
	return {
		executorReadAboutMe: () => dispatch(executorReadAboutMe())
	};
};

// Export
export default connect(mapStateToProps, mapDispatchToProps)(AboutMe);