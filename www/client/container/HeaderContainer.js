import Header from "../component/Header";
import { loginAuthenticationExecutor, init_login, validLogoutSession } from "../modules/Authentication";
import { connect } from "react-redux";


let mapStateToProps = (state) => {
	return {
		status: state.authentication.status,
		isLoggedIn: state.authentication.isLoggedIn
	};
}

let mapDispatchToProps = (dispatch) => {
	return {
		loginAuthenticationExecutor: (password) => dispatch(loginAuthenticationExecutor(password)),
		initLogin: () => dispatch(init_login({status:"INIT",isLoggedIn:false})),
		validLogoutSession: () => dispatch(validLogoutSession())
	};
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);