from flask_restful import Resource
from flask import request


class Post(Resource):

	def __init__(self, db, path):
		self.db = db
		self.path = path

	def get(self):
		params = {
			"_key": request.args.get("key")
		}
		rs = self.db.call_proc("SP_READ_POST", params)

		return rs.to_json()

	def post(self):
		req_data = request.get_json()
		params = {}
		resp = {}

		params["_key"] = ( None if req_data["key"] == "" else req_data["key"] )
		params["_title"] = req_data["title"]
		params["_content"] = req_data["content"]
		params["_is_posting"] = req_data["is_posting"]

		rs = self.db.call_proc("SP_INPUT_POST", params)

		resp["status"] = ("ERROR" if rs.to_json()["post_id"] == -1 else "SUCCEED")
		resp["msg"] = ("Error!" if resp["status"] == "ERROR" else "Save post")
		resp["post_id"] = rs.to_json()["post_id"]

		return resp

	def delete(self):
		resp = {}
		params = {
			"_key": request.args.get("key")
		}
		rs = self.db.call_proc("SP_DELETE_POST",params)

		resp["status"] = ("SUCCEED" if rs.to_json()["status"] else "ERROR")
		resp["msg"] = ("" if resp["status"] == "SUCCEED" else "Error!")

		return resp