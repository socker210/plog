from flask import request
from flask_restful import Resource

class Authentication(Resource):

	def __init__(self,db,path):
		self.db = db
		self.path = path

	def post(self):
		params = {
			"_password":request.get_json()["password"]
		}
		rs = self.db.call_proc("SP_COMPARE_PASSWORD",params)

		return rs.to_json()