from flask_restful import Resource
from flask import request


class Home(Resource):

	def __init__(self,db,path):
		self.db = db
		self.path = path

	def get(self):
		resp = {"post": []}
		rs = self.db.call_proc("SP_READ_HOME")

		for row in rs:
			tmp = {}
			for i in range(0,len(row)):
				tmp[rs.columns[i]] = row[i]
			resp["post"].append(tmp)

		return resp