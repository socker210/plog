from flask_restful import Resource


class PostList(Resource):
	def __init__(self, db, path):
		self.db = db
		self.path = path

	def get(self):
		rs = self.db.call_proc("SP_READ_ALL_POSTS")

		resp = {"data":[]}
		for row in rs:
			tmp = {}
			for idx in range(0, len(row)):
				tmp[rs.columns[idx]] = row[idx]
			resp["data"].append(tmp)


		return { "posts": resp }