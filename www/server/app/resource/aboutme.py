from flask import request
from flask_restful import Resource
import base64
import time

class AboutMe(Resource):

	def __init__(self, db, path):
		self.db = db
		self.path = path

	def _gen_filename(self):
		return str(int(round(time.time() * 1000)))

	def get(self):
		rs = self.db.call_proc("SP_READ_ABOUT_ME")

		# Make profile Path
		resp = rs.to_json()
		resp["path"] = (self.path.PROFILE_REL_IMG_PATH + "/" + resp["img_file_name"] if resp["is_user_image"] else "")

		del resp["img_file_name"]

		return resp

	def post(self):
		params = request.get_json()
		rs = self.db.call_proc("SP_INPUT_ABOUT_ME", params)

		resp = {
			"status": ("SUCCEED" if rs[0,0] == 1 else "FAILED"),
			"msg": ("Save Profile" if rs[0,0] == 1 else "Error")
		}

		return resp

	def put(self):
		action = request.get_json()["action"]
		params = {}

		if action == "UPLOAD":
			img = request.get_json()["img"].split(",")[1]
			nw_fn = self._gen_filename()
			path = ""

			# Create File
			path = self.path.PROFILE_REAL_IMG_PATH+"/"+nw_fn
			f = open(path,"w")
			f.write( base64.b64decode(img) )
			f.close()

			params = {
				"is_user_image": True,
				"img_file_name": nw_fn
			}
		else:
			params = {
				"is_user_image": False,
				"img_file_name": ""
			}

		# Update DB
		rs = self.db.call_proc("SP_UPDATE_PROFILE_IMG", params)

		return rs.to_json()
		