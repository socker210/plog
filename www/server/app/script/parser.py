import ConfigParser


class ReadConfig(object):


	def __init__(self, path):

		self.config = ConfigParser.ConfigParser()
		self.config.read(path)
	
	def get_config_to_string(self, session, opt):

		return self.config.get(session, opt)

	def get_config_to_list(self,session):

		return self.config.items(session)

	def get_config_to_dict(self,session):

		return dict((key,value) for key, value in self.get_config_to_list(session))