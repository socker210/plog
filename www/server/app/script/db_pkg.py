from sqlalchemy import create_engine
import parser
import os


# Engine
path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "db.ini")
config = parser.ReadConfig(path).get_config_to_dict("Connection")
DB_URL = "{driver}://{user}:{password}@{host}:{port}/{database}".format(driver=config["driver"], 
																	     user=config["user"], 
																		 password=config["password"], 
																		 host=config["host"], 
																		 port=config["port"], 
																		 database=config["database"])
engine = create_engine(DB_URL)








# Access to database
class AccessDB(object):
	
	def __repr__(self):
		global DB_URL

		return DB_URL

	def _get_connection(self):
		global engine

		return engine.raw_connection()

	def _close_connection(self, cursor, conn):
		cursor.close()
		conn.close()

	def call_proc(self, proc, param=None):
		conn = self._get_connection()
		c = conn.cursor()

		try:
			c.callproc(proc, param)
			conn.commit()

			raw_data = c.fetchall()
			columns = list((item.name) for item in c.description)
			return DataRow(raw_data, columns)
		
		except Exception as e:
			raise e

		finally:
			self._close_connection(c, conn)





# Database data type
class DataRow(object):
	

	def __init__(self, raw_data, columns):
		self.raw_data = raw_data
		self.columns = columns

	def __getitem__(self, key):
		row = None
		col = None

		try:
			#Check
			if not (isinstance(key, tuple) or isinstance(key, int)):
				raise Exception("DataRow key error")
			if isinstance(key, tuple) and len(key) >= 3:
				raise Exception("The number of key is out of range")
				
			# Set row and col
			row = key[0] if isinstance(key, tuple) else key
			col = key[1] if isinstance(key, tuple) else col

			# Compare type of row and col
			if not isinstance(row, int):
				raise Exception("Row data type error. Row data type must be int.")
			if col != None and not (isinstance(col, int) or isinstance(col,str)):
				raise Exception("Col data type error. Col data type must be int or str.")

			# Return data
			if col != None:
				col = self.columns.index(col) if isinstance(col, str) else col
				return self.raw_data[row][col]
			else:
				return self.raw_data[row]

		except Exception as e:
			raise e

	def has_column(self, key):
		try:
			self.columns.index(key)
			return True

		except:
			return False

	def to_json(self):
		json = {}

		for i in range(0, len(self.raw_data)):
			for c in self.columns:
				json[c] = self[i, c]

		return json