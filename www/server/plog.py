import configuration
from app import AccessDB, DataRow
from app import AboutMe, Post, PostList, Home, Authentication

from flask import Flask
from flask_restful import Api


# Application
db = AccessDB()
app = Flask(__name__)
api = Api(app)

# Add Resource
api.add_resource(AboutMe, "/api/me", resource_class_kwargs={"db": db, "path": configuration.Path})
api.add_resource(Post, "/api/post", resource_class_kwargs={"db": db, "path": configuration.Path})
api.add_resource(PostList, "/api/posts", resource_class_kwargs={"db": db, "path": configuration.Path})
api.add_resource(Home, "/api/home", resource_class_kwargs={"db": db, "path": configuration.Path})
api.add_resource(Authentication, "/api/authentication", resource_class_kwargs={"db": db, "path": configuration.Path})


if __name__ == "__main__":
	app.run(debug=True)