var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var CleanWebpackPlugin = require("clean-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
	entry: __dirname + "/www/client/index.js",
	output: {
		path: __dirname + "/www/dist",
		publicPath: "/",
		filename: "bundle.js"
	},
	module: {
		rules: [{
			test: /\.js?/,
			exclude: /node_modules/,
			use: [{
				loader: "babel-loader",
				query: {
					presets: ["es2015", "react"]
				}
			}]
		},
		{
			test: /\.css?/,
			use: ["style-loader", {
				loader: "css-loader",
				query: {
					modules: true,
					localIdentName: "[local]"
				}
			}]
		},
		{
			test: /\.(svg|ttf|woff|woff2|eot)$/,
			use: {
				loader: "url-loader",
				query: {
					limit: 5000,
					name: "asset/fonts/[name]__[hash].[ext]"
				}
			}
		}]
	},
	plugins: [
		new CleanWebpackPlugin(["www/dist"],{
			root: __dirname,
			exclude: ["resources"]
		}),
		new CopyWebpackPlugin([
			{from: "client/plugins", to: "plugins"},
			{from: "client/static/ckeditor.css", to: "asset/static"},
			{from: "../node_modules/bootstrap/dist/css/bootstrap.min.css", to: "asset/static"}
		],
		{
			context: "www"
		}),
		new HtmlWebpackPlugin({
			title: "Programming log",
			hash: true,
			favicon: "www/asset/favicon.ico",
			template: "www/asset/template.html"
		})
	]
}